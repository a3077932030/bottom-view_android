package com.ohuang.bottomtab;

import android.content.Context;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;


public class NavigationView extends FrameLayout implements View.OnClickListener {
    private Context context;
    private List<CustomNavigationTab> tabList;
    private OnClickNavigationListener onClickNavigationListener;
    private OnSelectListener onSelectListener;
    private onUnselectListener onUnselectListener;
    private int selectPosition = 0;

    public NavigationView(Context context) {
        this(context, null);
    }

    public NavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }


    private void initView(Context context) {
        this.context = context;

    }

    public List<CustomNavigationTab> getTabList() {
        return tabList;
    }

    /**
     * 添加List<NavigationViewTab>   NavigationViewTab使用NavigationBuilder创建
     *
     * @param tabList
     */
    public void setTabList(List<CustomNavigationTab> tabList) {
        this.tabList = tabList;
        removeAllViews();
        for (CustomNavigationTab navigationTab : tabList) {
            View navigationButton = navigationTab.createView(this);
            navigationButton.setOnClickListener(this);
            addView(navigationButton,new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        }
        selectButton(selectPosition);
    }

    /**
     * 添加NavigationViewTab   NavigationViewTab使用NavigationBuilder创建
     *
     * @param navigationTab
     * @return
     */
    public NavigationView addTab(CustomNavigationTab navigationTab) {
        if (tabList == null) {
            tabList = new ArrayList<>();
        }
        if (navigationTab != null) {
            tabList.add(navigationTab);
            View navigationButton = navigationTab.createView(this);
            navigationButton.setOnClickListener(this);

            addView(navigationButton);
            if (selectPosition == tabList.size() - 1) {
                selectButton(selectPosition);
            }
        }
        return this;
    }


    public int getSelectPosition() {
        return selectPosition;
    }

    /**
     * 设置选中的按钮
     *
     * @param p
     */
    public void setSelect(int p) {
        cancelSelect(selectPosition);
        this.selectPosition = p;
        selectButton(selectPosition);
    }

    public OnClickNavigationListener getOnClickNavigationListener() {
        return onClickNavigationListener;
    }

    /**
     * 按钮点击监听事件
     *
     * @param onClickNavigationListener
     */
    public void setOnClickNavigationListener(OnClickNavigationListener onClickNavigationListener) {
        this.onClickNavigationListener = onClickNavigationListener;
    }

    public NavigationView.onUnselectListener getOnUnselectListener() {
        return onUnselectListener;
    }

    public void setOnUnselectListener(NavigationView.onUnselectListener onUnselectListener) {
        this.onUnselectListener = onUnselectListener;
    }

    public OnSelectListener getOnSelectListener() {
        return onSelectListener;
    }

    /**
     * 选中监听事件
     *
     * @param onSelectListener return false 拦截可选中事件
     */
    public void setOnSelectListener(OnSelectListener onSelectListener) {
        this.onSelectListener = onSelectListener;
    }

    public NavigationBuilder getNavigationBuilder() {
        return new NavigationBuilder();
    }

    private long delayTime=200;
    private long lastTime=0;

    public long getDelayTime() {
        return delayTime;
    }

    /**
     * 设置防抖延时时间
     * @param delayTime
     */
    public void setDelayTime(long delayTime) {
        this.delayTime = delayTime;
    }

    @Override
    public void onClick(View component) {
        if (SystemClock.uptimeMillis()-lastTime<delayTime){
            return;
        }
        lastTime=SystemClock.uptimeMillis();
        for (int i = 0; i < getChildCount(); i++) {
            if (component == getChildAt(i)) {
                if (onClickNavigationListener != null) {
                    onClickNavigationListener.onClick(this, i);
                }
                if (selectPosition != i) {
                    if (onSelectListener == null || !onSelectListener.onSelect(this, i)) {
                        cancelSelect(selectPosition);
                        if (onUnselectListener != null) {
                            onUnselectListener.onUnselect(this, selectPosition);
                        }
                        selectButton(i);
                        selectPosition = i;
                    }
                }
                break;
            }
        }
    }


    private void cancelSelect(int position) {
        if (tabList == null) {
            return;
        }
        if (position > -1 && position < tabList.size()) {
            NavigationButton navigationButton = (NavigationButton) getChildAt(position);
            tabList.get(position).setNormal(navigationButton);
        }
    }

    private void selectButton(int position) {
        if (tabList == null) {
            return;
        }
        if (position > -1 && position < tabList.size()) {
            NavigationButton navigationButton = (NavigationButton) getChildAt(position);
            tabList.get(position).setSelect(navigationButton);

        }
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (tabList != null) {
            int with = r - l-getPaddingLeft()-getPaddingRight();
            int height = b - t;
            int count = getChildCount();
            for (int j = 0; j < count; j++) {
                NavigationButton navigationButton = (NavigationButton) getChildAt(j);
                navigationButton.layout((int) ( j * (1.0f * with / count))+getPaddingLeft(), getPaddingTop(),  (j + 1) * with / count+getPaddingLeft(),  height-getPaddingBottom());
            }
        }
    }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        for (int i = 0; i < getChildCount(); i++) {
//            int with = MeasureSpec.getSize(widthMeasureSpec) / getChildCount();
//            int withMsp = MeasureSpec.makeMeasureSpec(with, MeasureSpec.UNSPECIFIED);
//            getChildAt(i).measure(withMsp, heightMeasureSpec);
//
//        }
//
//    }

    public interface OnClickNavigationListener {
        void onClick(NavigationView navigationView, int index);
    }

    public interface OnSelectListener {
        /**
         * 按钮选中事件
         *
         * @param navigationView
         * @param index          选中按钮的位置
         * @return true 可拦截选中事件
         */
        boolean onSelect(NavigationView navigationView, int index);
    }

    public interface onUnselectListener {
        void onUnselect(NavigationView navigationView, int index);
    }
}
