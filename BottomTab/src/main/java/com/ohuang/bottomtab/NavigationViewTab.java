package com.ohuang.bottomtab;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;

public class NavigationViewTab implements CustomNavigationTab {
    NavigationTab navigationTab;
    NavigationTab selectNavigationTab;

    public NavigationViewTab(NavigationTab navigationTab, NavigationTab selectNavigationTab) {
        this.navigationTab = navigationTab;
        this.selectNavigationTab = selectNavigationTab;
    }

    @Override
    public View createView(ViewGroup viewGroup) {
        return navigationTab.create(viewGroup.getContext());
    }

    @Override
    public void setNormal(View view) {
        navigationTab.setNavigationButton(view);
    }

    @Override
    public void setSelect(View view) {
        selectNavigationTab.setNavigationButton(view);
    }

    public static class Builder {
        NavigationBuilder navigationBuilder;
        NavigationBuilder selectNavigationBuilder;

        public Builder(NavigationBuilder navigationBuilder, NavigationBuilder selectNavigationBuilder) {
            this.navigationBuilder = navigationBuilder.copy();
            this.selectNavigationBuilder = selectNavigationBuilder.copy();
        }

        public Builder setImageSize(int with, int height) {
            navigationBuilder.setImageSize(with, height);
            return this;
        }

        public Builder setSelectImageSize(int with, int height) {
            selectNavigationBuilder.setImageSize(with, height);
            return this;
        }


        /**
         * 按钮文字颜色
         *
         * @param textColor
         * @return
         */
        public Builder setTextColor(int textColor) {
            navigationBuilder.setTextColor(textColor);
            return this;
        }

        public Builder setSelectTextColor(int textColor) {
            selectNavigationBuilder.setTextColor(textColor);
            return this;
        }


        /**
         * true为隐藏图片
         *
         * @param b
         * @return
         */
        public Builder NoImage(boolean b) {
            navigationBuilder.NoImage(b);
            return this;
        }

        public Builder NoSelectImage(boolean b) {
            selectNavigationBuilder.NoImage(b);
            return this;
        }

        /**
         * true为隐藏文字
         *
         * @param b
         * @return
         */
        public Builder NoText(boolean b) {
            navigationBuilder.NoText(b);
            return this;
        }

        public Builder NoSelectText(boolean b) {
            selectNavigationBuilder.NoText(b);
            return this;
        }

        /**
         * 图标
         *
         * @param drawable 默认图片的资源Id
         * @return
         */
        public Builder setDrawable(Drawable drawable) {
            navigationBuilder.setDrawable(drawable);
            return this;
        }

        public Builder setSelectDrawable(Drawable drawable) {
            selectNavigationBuilder.setDrawable(drawable);
            return this;
        }


        /**
         * 文字和图片之间的空间大小
         *
         * @param space
         * @return
         */
        public Builder setSpace(int space) {
            navigationBuilder.setSpace(space);
            return this;
        }

        public Builder setSelectSpace(int space) {
            selectNavigationBuilder.setSpace(space);
            return this;
        }

        /**
         * 顶部空间
         *
         * @param space
         * @return
         */
        public Builder setSpaceTop(int space) {
            navigationBuilder.setSpaceTop(space);
            return this;
        }

        public Builder setSelectSpaceTop(int space) {
            selectNavigationBuilder.setSpaceTop(space);

            return this;
        }


        /**
         * 底部空间
         *
         * @param space
         * @return
         */
        public Builder setSpaceBottom(int space) {
            navigationBuilder.setSpaceBottom(space);
            return this;
        }

        public Builder setSelectSpaceBottom(int space) {
            selectNavigationBuilder.setSpaceBottom(space);
            return this;
        }


        /**
         * 设置文字
         *
         * @param text
         * @return
         */
        public Builder setText(String text) {
            navigationBuilder.setText(text);
            return this;
        }

        public Builder setSelectText(String text) {
            selectNavigationBuilder.setText(text);
            return this;
        }

        /**
         * 文字大小
         *
         * @param textSize
         * @return
         */
        public Builder setTextSize(int textSize) {
            navigationBuilder.setTextSize(textSize);
            return this;
        }

        public Builder setSelectTextSize(int textSize) {
            selectNavigationBuilder.setTextSize(textSize);
            return this;
        }

        /**
         * 添加数据
         *
         * @param data
         */
        public Builder setData(Object data) {
            this.navigationBuilder.setData(data);
            return this;
        }

        public Builder setSelectData(Object data) {
            this.selectNavigationBuilder.setData(data);
            return this;
        }

        public Builder setNavigationButtonSetting(NavigationBuilder.NavigationButtonSetting navigationButtonSetting) {
            this.navigationBuilder.setNavigationButtonSetting(navigationButtonSetting);
            return this;
        }

        public Builder setSelectNavigationButtonSetting(NavigationBuilder.NavigationButtonSetting navigationButtonSetting) {
            this.selectNavigationBuilder.setNavigationButtonSetting(navigationButtonSetting);
            return this;
        }

        public NavigationViewTab build() {
            return new NavigationViewTab(navigationBuilder.build(), selectNavigationBuilder.build());
        }

    }
}
