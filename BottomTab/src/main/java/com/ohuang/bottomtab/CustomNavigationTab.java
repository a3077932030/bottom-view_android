package com.ohuang.bottomtab;

import android.view.View;
import android.view.ViewGroup;

public  interface CustomNavigationTab {

   View createView(ViewGroup viewGroup);

    void setNormal(View view);

    void setSelect(View view);
}
