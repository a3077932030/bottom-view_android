package com.ohuang.bottomtab;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

public class NavigationTab {
    private int textColor;

    private Object data;
    private int imgVisible = View.VISIBLE;

    private int textVisible = View.VISIBLE;
    private Drawable defaultDrawable;


    private int space = 0;
    private int spaceTop = 0;
    private int spaceBottom = 0;

    private String text;
    private int textSize = -1;

    private int imgw = 0, imgh = 0;


    NavigationBuilder.NavigationButtonSetting navigationButtonSetting;

    public NavigationTab(NavigationBuilder navigationBuilder) {
        textColor = navigationBuilder.textColor;
        textSize=navigationBuilder.textSize;
        navigationButtonSetting = navigationBuilder.navigationButtonSetting;
        imgVisible = navigationBuilder.ImgVisible;
        textVisible = navigationBuilder.TextVisible;
        defaultDrawable = navigationBuilder.drawable;

        space = navigationBuilder.space;
        spaceTop = navigationBuilder.spaceTop;
        spaceBottom = navigationBuilder.spaceBottom;
        data = navigationBuilder.data;
        imgh = navigationBuilder.imgh;
        imgw = navigationBuilder.imgw;
        text = navigationBuilder.text;

    }


    public int getTextColor() {
        return textColor;
    }


    public int getImgVisible() {
        return imgVisible;
    }

    public int getTextVisible() {
        return textVisible;
    }


    public Drawable getDefaultDrawable() {
        return defaultDrawable;
    }


    public int getSpace() {
        return space;
    }

    public Object getData() {
        return data;
    }

    public String getText() {
        return text;
    }

    public int getTextSize() {
        return textSize;
    }

    public int getImgw() {
        return imgw;
    }

    public int getImgh() {
        return imgh;
    }

    public int getSpaceTop() {
        return spaceTop;
    }

    public int getSpaceBottom() {
        return spaceBottom;
    }

    public View create(Context context) {
        NavigationButton navigationButton = new NavigationButton(context);
        setNavigationButton(navigationButton);
        return navigationButton;
    }

    public void setNavigationButton(View view) {
        if (view instanceof NavigationButton) {
            NavigationButton navigationButton= (NavigationButton) view;
            navigationButton.setSpace(space);
            navigationButton.setSpaceTop(spaceTop);
            navigationButton.setSpaceBottom(spaceBottom);

            navigationButton.setDrawable(defaultDrawable);
            navigationButton.setImageSize(imgw, imgh);
            navigationButton.setText(text);
            navigationButton.setTextSize(textSize);
            navigationButton.setTextColor(textColor);

            navigationButton.setImageVisible(imgVisible);
            navigationButton.setTextVisible(textVisible);
            if (navigationButtonSetting != null) {
                navigationButtonSetting.onSetValue(navigationButton);
            }
        }
    }
}
