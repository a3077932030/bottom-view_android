package com.ohuang.bottomtab;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


public class NavigationButton extends FrameLayout {
    public TextView text;
    public ImageView image;
    public View space;
    public View spaceTop;
    public View spaceBottom;

    public NavigationButton(@NonNull Context context) {
        this(context, null);
    }

    public NavigationButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NavigationButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }


    private void initView(Context context) {
        LayoutInflater.from(context).inflate(R.layout.oh_navigation_tab,this,true);
        text = findViewById(R.id.tv_title);
        image = findViewById(R.id.image_icon);
        space = findViewById(R.id.centerView);
        spaceBottom = findViewById(R.id.bottomView);
        spaceTop = findViewById(R.id.topView);

    }


    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int with = right - left;
        int height = bottom - top;
        getChildAt(0).layout(0, 0, with, height);
    }



    public void setText(String s) {
        if (s != null) {
            text.setText(s);
        }
    }

    public void setTextColor(int c) {
        text.setTextColor(c);
    }

    public void setTextSize(int size) {
        if (size > -1) {
            text.setTextSize(TypedValue.COMPLEX_UNIT_PX,size);
        }
    }


    public void setImage(int id) {
        if (id != 0) {
            image.setBackgroundResource(id);
        }
    }

    public void setDrawable(Drawable drawable){
        image.setImageDrawable(drawable);
    }

    public void setImageSize(int with, int height) {
        if (with < 0) {
            with = 0;
        }
        if (height < 0) {
            height = 0;
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(with, height);
        image.setLayoutParams(layoutParams);
    }

    public void setSpace(int height) {
        if (height >= 0) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, height);
            space.setLayoutParams(layoutParams);
        }
    }

    public void setSpaceTop(int height) {
        if (height >= 0) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, height);
            spaceTop.setLayoutParams(layoutParams);
        }
    }


    public void setSpaceBottom(int height) {
        if (height >= 0) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, height);
            spaceBottom.setLayoutParams(layoutParams);
        }
    }



    public void setImageVisible(int visible) {
        image.setVisibility(visible);
    }

    public void setTextVisible(int visible) {
        text.setVisibility(visible);
    }


}
