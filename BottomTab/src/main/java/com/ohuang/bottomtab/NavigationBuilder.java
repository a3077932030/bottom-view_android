package com.ohuang.bottomtab;


import android.graphics.drawable.Drawable;
import android.view.View;

public class NavigationBuilder {
    int textColor = 0xff000000;

    Object data;

    int ImgVisible = View.VISIBLE;
    int TextVisible = View.VISIBLE;
    Drawable drawable;


    int space = 0;

    int spaceTop = 0;
    int spaceBottom = 0;


    int imgw = 100, imgh = 100;

    String text;
    int textSize = -1;
    boolean noText = false;
    boolean noImage = false;
    NavigationButtonSetting navigationButtonSetting;

    /**
     * 设置显示的图片大小
     *
     * @param with
     * @param height
     * @return
     */
    public NavigationBuilder setImageSize(int with, int height) {
        if (with >= 0) {
            imgw = with;
        }
        if (height >= 0) {
            imgh = height;
        }
        return this;
    }


    /**
     * 按钮文字颜色
     *
     * @param textColor
     * @return
     */
    public NavigationBuilder setTextColor(int textColor) {
        this.textColor = textColor;
        return this;
    }


    /**
     * true为隐藏图片
     *
     * @param b
     * @return
     */
    public NavigationBuilder NoImage(boolean b) {
        noImage = b;
        if (b) {
            ImgVisible = View.GONE;
        } else {
            ImgVisible = View.VISIBLE;
        }

        return this;
    }

    /**
     * true为隐藏文字
     *
     * @param b
     * @return
     */
    public NavigationBuilder NoText(boolean b) {
        noText = b;
        if (b) {
            TextVisible = View.GONE;
        } else {
            TextVisible = View.VISIBLE;
        }
        return this;
    }

    /**
     * 图标
     *
     * @param drawable 默认图片的资源Id
     * @return
     */
    public NavigationBuilder setDrawable(Drawable drawable) {
        this.drawable = drawable;
        return this;
    }


    /**
     * 文字和图片之间的空间大小
     *
     * @param space
     * @return
     */
    public NavigationBuilder setSpace(int space) {
        this.space = space;
        return this;
    }

    /**
     * 顶部空间
     *
     * @param space
     * @return
     */
    public NavigationBuilder setSpaceTop(int space) {
        this.spaceTop = space;
        return this;
    }


    /**
     * 底部空间
     *
     * @param space
     * @return
     */
    public NavigationBuilder setSpaceBottom(int space) {
        this.spaceBottom = space;
        return this;
    }


    /**
     * 设置文字
     *
     * @param text
     * @return
     */
    public NavigationBuilder setText(String text) {
        this.text = text;
        return this;
    }

    /**
     * 文字大小
     *
     * @param textSize
     * @return
     */
    public NavigationBuilder setTextSize(int textSize) {
        this.textSize = textSize;
        return this;
    }

    /**
     * 添加数据
     *
     * @param data
     */
    public NavigationBuilder setData(Object data) {
        this.data = data;
        return this;
    }

    public NavigationBuilder setNavigationButtonSetting(NavigationButtonSetting navigationButtonSetting){
        this.navigationButtonSetting=navigationButtonSetting;
        return this;
    }

    public NavigationBuilder copy() {
        return new NavigationBuilder().setImageSize(imgw, imgh)
                .setDrawable(drawable)
                .setTextColor(textColor)
                .setText(text)
                .setTextSize(textSize)
                .setSpaceTop(spaceTop)
                .setSpace(space)
                .setSpaceBottom(spaceBottom)
                .setData(data)
                .NoImage(noImage)
                .NoText(noText)
                .setNavigationButtonSetting(navigationButtonSetting);
    }

    public NavigationTab build() {
        return new NavigationTab(this);
    }

    public interface NavigationButtonSetting{

        void onSetValue(NavigationButton navigationButton);
    }
}
