package com.ohuang.bottombar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.ohuang.bottomtab.CustomNavigationTab;
import com.ohuang.bottomtab.NavigationBuilder;
import com.ohuang.bottomtab.NavigationTab;
import com.ohuang.bottomtab.NavigationView;
import com.ohuang.bottomtab.NavigationViewTab;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigationView = findViewById(R.id.na_main);
        NavigationBuilder navigationBuilder = new NavigationBuilder()
                .setDrawable(getDrawable(R.drawable.ic_launcher_background))
                .setSpaceTop(20)
                .setSpaceBottom(20);

        NavigationBuilder selcetNvBuilder = navigationBuilder.copy()
                .setDrawable(getDrawable(R.drawable.ic_launcher_foreground))
                .setTextColor(0xff00ffff);

        NavigationViewTab navigationTab = new NavigationViewTab.Builder(navigationBuilder, selcetNvBuilder).setText("a123456789").setSelectText("a123456789").build();
        NavigationViewTab navigationTab1 = new NavigationViewTab.Builder(navigationBuilder, selcetNvBuilder).setText("b123456789").setSelectText("b123456789").build();
        NavigationViewTab navigationTab2 = new NavigationViewTab.Builder(navigationBuilder, selcetNvBuilder).setText("c").setSelectText("c").build();
        List<CustomNavigationTab> list = new ArrayList<>();
        list.add(navigationTab);
        list.add(navigationTab1);
        list.add(navigationTab2);

        navigationView.setTabList(list);
        navigationView.setOnSelectListener(new NavigationView.OnSelectListener() {
            @Override
            public boolean onSelect(NavigationView navigationView, int index) {
                Log.d("TAG", "onSelect: " + index);
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();


    }
}